import modules.scripts as scripts
import gradio as gr
import os

from modules.processing import process_images, Processed
from modules.shared import device

from modules.sd_samplers import samplers

import k_diffusion.sampling
import torch

class Script(scripts.Script):

    '''
    from Birch-san's karras noise scheduler implementation
    https://github.com/Birch-san/stable-diffusion/blob/birch-mps-waifu/scripts/txt2img_fork.py

    use noise scheduler from arXiv:2206.00364, which is implemented for k-diffusion samplers,
    but probably should only used with samplers introduced in the same paper, which are
    - Heun
    - Euler
    - DPM2
    '''

    def title(self):
        return "Karras Noise Scheduler"

    def show(self, is_img2img):
        return scripts.AlwaysVisible

    def ui(self, is_img2img):

        # preset list settings
        # [sigma_min, sigma_max, rho]
        preset_list = {
            'Default'                   : [0.0292, 14.6146, 7.],
            'Better Fine Details'       : [0.0292, 14.6146, 10.],
            'Better Coarse Details'     : [0.0292, 14.6146, 5.],
            'DPM-Solver++ 2M 5 Steps'   : [0.1072, 14.6146, 9.],
            'Automatic1111 Default'     : [0.1   , 10.    , 7.]
        }

        with gr.Accordion(
            'Karras Noise Scheduler',
            open = False
        ):

            # setting preset
            preset = gr.Dropdown(
                label = 'Preset',
                value = None,
                choices = [preset for preset in preset_list.keys()],
                interactive = True
            )

            with gr.Row():

                # set this higher for low step counts, so it will focus on middle or high sigmas (to prioritize pose or body shape, at the expense of face and hair details)
                sigma_min = gr.Slider(
                    value = 0.0292,
                    label = 'sigma min',
                    minimum = 0.,
                    maximum = 100,
                    step = 0.001
                )

                # set this lower for low step counts, so it will focus on middle or low sigmas (to prioritize face or hair details, at the expense of pose and body shape)
                sigma_max = gr.Slider(
                    value = 14.6146,
                    label = 'sigma max',
                    minimum = 0.,
                    maximum = 100,
                    step = 0.001
                )

                # rho = 10 samples more from low sigmas (<1.0); good for making faces coherent
                # rho = 5 spends more time on high sigmas (>2.0); good for pose and body shape
                rho = gr.Slider(
                    value = 7.,
                    label = 'rho',
                    minimum = 0.,
                    maximum = 100,
                    step = 0.01
                )

            with gr.Row():

                always_on = gr.Checkbox(
                    value = False,
                    label = 'Always use karras noise scheduler, even for non-karras samplers'
                )

                # ramp from sigma_max to sigma slightly above sigma_min, cuz with low step counts, every sigmas matter
                early_ramp = gr.Checkbox(
                    value = True,
                    label = 'Karras noise early ramp'
                )

        preset.change(
            fn = lambda p: preset_list[p],
            inputs = [preset],
            outputs = [sigma_min, sigma_max, rho]
        )

        return [sigma_min, sigma_max, rho, early_ramp, preset, always_on]

    def process(self, p, sigma_min, sigma_max, rho, early_ramp, preset, always_on):

        # i'm not gonna lie to u, but idk whether this is the proper way to do this?
        # cuz with normal scripts u can just dump all of this in run()
        # but so far i think this works

        SAMPLERS = [sampler.name for sampler in samplers]
        KARRAS_SAMPLERS = ['Euler', 'Heun', 'DPM2']
        KARRAS_SAMPLERS_INDEX = [SAMPLERS.index(sampler) for sampler in KARRAS_SAMPLERS]

        noise_scheduler_fn = k_diffusion.sampling.get_sigmas_karras
        device = 'cuda' if torch.cuda.is_available() else 'cpu'

        def get_premature_sigma_min(
            steps: int,
            sigma_max: float,
            sigma_min_nominal: float,
            rho: float
        ) -> float:
            min_inv_rho = sigma_min_nominal ** (1 / rho)
            max_inv_rho = sigma_max ** (1 / rho)
            ramp = (steps - 2) * 1 / (steps - 1)
            sigma_min = (max_inv_rho + ramp * (min_inv_rho - max_inv_rho)) ** rho
            return sigma_min

        if early_ramp:
            sigma_min = get_premature_sigma_min(
                steps = p.steps + 1,
                sigma_max = sigma_max,
                sigma_min_nominal = sigma_min,
                rho = rho
            )

        def substitute_noise_scheduler(steps):
            return noise_scheduler_fn(
                n = steps,
                sigma_min = sigma_min,
                sigma_max = sigma_max,
                rho = rho,
                device = device
            )

        if always_on:
            p.sampler_noise_scheduler_override = substitute_noise_scheduler
        elif p.sampler_index in KARRAS_SAMPLERS_INDEX:
            p.sampler_noise_scheduler_override = substitute_noise_scheduler

    def run(self, p, sigma_min, sigma_max, rho, early_ramp, preset, always_on):

        return process_images(p)
