# Karras Noise Scheduler for Automatic1111 webui

implementation of [Birch-san's karras noise scheduler implementation]
(https://github.com/Birch-san/stable-diffusion/blob/birch-mps-waifu/scripts/txt2img_fork.py), which also includes the early noise ramp thingy (using the sigma right after sigma min)

most of the code here is just me copying and pasting the code from Birch-san's fork, and making it a script that can be used with Automatic1111's webui

karras noise scheduler is a noise scheduler from arXiv:2206.00364, which is implemented for k-diffusion samplers,
but probably should only used with samplers introduced in the same paper, which are
- Heun
- Euler
- DPM2

with this extension, those three samplers will automatically use karras noise scheduler

why do this even exist? cuz i think the early noise ramp thingy is beneficial for me, cuz i mostly do 8 steps when generating images, other than that, the default settings for karras noise scheduler in Automatic1111 is different from Birch-san's implementation, [and apparently the results won't converge if we're using the wrong settings](https://github.com/AUTOMATIC1111/stable-diffusion-webui/commit/b34b25b4c941819d34f29be6c4c1ec01e64585b4#commitcomment-86212482)
